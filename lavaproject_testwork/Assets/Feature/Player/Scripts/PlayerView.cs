﻿using System;
using MCOV;
using UnityEngine;
using UnityEngine.AI;

namespace Feature.Player.Scripts
{
    
    public class PlayerView : AObject<PlayerController>
    {
        [SerializeField]
        private NavMeshAgent agent;

        [SerializeField]
        private Animator animator;

        [SerializeField]
        private Vector3 startPosition;

        private float _timeTask;

        private bool _isEndMove;

        private PlayerTask _currentTask;

        protected override void Show()
        {
            agent.Move(startPosition);
        }

        private void Update()
        {
            if (_timeTask > 0)
            {
                _timeTask -= Time.deltaTime;
            }

            if (_currentTask != null)
            {
                if (_currentTask.Position != null)
                {
                    var delta = Vector3.Distance(transform.position, _currentTask.Position.Value);
                    

                    _isEndMove = delta <= 0.1;
                }
                else
                {
                    _isEndMove = true;
                }
            }

            
            if (_isEndMove && _timeTask <= 0 && _currentTask != null)
            {
                EndCurrentTask();
            }

        }

        protected override void Subscribe()
        {
            Controller.OnStartInteract += StartInteract;
        }

        protected override void Unsubscribe()
        {
            Controller.OnStartInteract -= StartInteract;
        }

        private void StartInteract(Vector3 position, Action actionEnd = null)
        {
            StopCurrentTask();
            
            var taskMoveHome = new PlayerTask();
            taskMoveHome.Position = startPosition;
            taskMoveHome.MinTime = 0f;
            taskMoveHome.ActionEnd = StopCurrentTask;
            taskMoveHome.AnimationName = "Run";
            
            var taskInteract = new PlayerTask();
            taskInteract.Position = null;
            taskInteract.MinTime = 3f;
            taskInteract.ActionEnd = actionEnd + delegate { StartTask(taskMoveHome);  };
            taskInteract.AnimationName = "Eat";
            
            var taskMove = new PlayerTask();
            taskMove.Position = position;
            taskMove.MinTime = 0;
            taskMove.ActionEnd =  delegate { StartTask(taskInteract);  };
            taskMove.AnimationName = "Run";


            StartTask(taskMove);
        }

        private void StartTask(PlayerTask playerTask)
        {
            _currentTask = playerTask;
            agent.isStopped = false;
            animator.SetBool(playerTask.AnimationName, true);
            _timeTask = playerTask.MinTime;
            _isEndMove = true;
            if(playerTask.Position != null)
                agent.SetDestination(playerTask.Position.Value);
        }
        
        private void StopCurrentTask()
        {
            if(_currentTask == null)
                return;
            
            agent.isStopped = true;
            animator.SetBool(_currentTask.AnimationName, false);
            _currentTask = null;
        }

        private void EndCurrentTask()
        {
            if(_currentTask == null)
                return;
            
            animator.SetBool(_currentTask.AnimationName, false);
            _currentTask.ActionEnd?.Invoke(); ;
        }
    }

    public class PlayerTask 
    {
        public Vector3? Position;
        public float MinTime;
        public Action ActionEnd;
        public string AnimationName;
    }


}