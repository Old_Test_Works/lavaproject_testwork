﻿using System;
using MCOV;
using UnityEngine;

namespace Feature.Player.Scripts
{
    public class PlayerController : AController
    {
        public event Action<Vector3, Action> OnStartInteract;
        
        public void MoveTo(Vector3 position, Action actionEnd)
        {
            OnStartInteract?.Invoke(position, actionEnd);
        }

        
    }
}