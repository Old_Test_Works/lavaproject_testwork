﻿using MCOV;
using UnityEngine;

namespace Feature.Position.Scripts
{
    
    public class GridPositionCalculator : AController, IPositionCalculator
    {
        //Todo пока что инджект моделей и асетов происходит через SerializeField
        [SerializeField]
        private GridPositionSettings settings;
        
        public Vector3 CalculatePosition(int id)
        {
            int countLine = id / settings.CountFixedCell;
            int countInLine = id % settings.CountFixedCell;
            
            if (settings.FixedX)
            {
                return new Vector3(countLine * settings.SizeCell.x,0, countInLine * settings.SizeCell.y);
            }

            return new Vector3(countInLine * settings.SizeCell.y,0, countLine * settings.SizeCell.x);
        }
    }

    //тобы можно было настраивать разные типы генерации 
    public interface IPositionCalculator : IController
    {
        public Vector3 CalculatePosition(int id);
    }
}

