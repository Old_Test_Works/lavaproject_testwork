﻿using UnityEngine;

namespace Feature.Position.Scripts
{
    //Todo Потом можно написать кастомное отображение
    [CreateAssetMenu(menuName = "Feature/Position/GridPositionSettings", fileName = "GridPositionSettings")]
    public class GridPositionSettings : ScriptableObject
    {
        [SerializeField]
        private bool fixedX = true;

        [SerializeField]
        private int countFixedCell = 5;

        [SerializeField]
        private Vector2 sizeCell = new Vector2(2,2);
        
        
        public bool FixedX => fixedX;
        
        public int CountFixedCell => countFixedCell;
        
        public Vector2 SizeCell => sizeCell;

    }

}