﻿using System;
using UnityEngine;

namespace Feature.Camera
{
    public class CameraInput : MonoBehaviour
    {
        [SerializeField] private float speed = 1f;

        [SerializeField] private Vector3 startPos;
        private Vector3 position = new Vector3();

        private void Start()
        {
            transform.position = startPos;
        }

        //Todo треш конечно но это не цель задания и сделано для удобства
        private void Update()
        {
            position = transform.position;
            if (Input.GetKey(KeyCode.W))
            {
                position.z += speed;
            }

            if (Input.GetKey(KeyCode.S))
            {
                position.z -= speed;
            }

            if (Input.GetKey(KeyCode.D))
            {
                position.x += speed;
            }

            if (Input.GetKey(KeyCode.A))
            {
                position.x -= speed;
            }

            transform.position = position;
        }
    }
}