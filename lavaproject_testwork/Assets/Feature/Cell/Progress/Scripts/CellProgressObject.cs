﻿using Feature.Cell.Cell.Scripts;
using MCOV;
using UnityEngine;
using UnityEngine.UI;

namespace Feature.Cell.Progress
{

    public class CellProgressObject : AObject<ProgressController>, ICellObject
    {
        [SerializeField]
        private Text textProgress;
        
        [SerializeField]
        private Text textButton;
        
        [SerializeField]
        private Image imageProgress;

        [SerializeField]
        private Button collectButton;

        [SerializeField]
        private Transform spawnContent;

        private ProgressRewardView _spawnedObject;
        
        private int _cellId;
        protected override void Subscribe()
        {
            collectButton.onClick.AddListener(ClickCollect);
            Controller.OnChangeTimeEntity += ChangeTimeEntityReaction;
        }


        protected override void Unsubscribe()
        {
            collectButton.onClick.RemoveListener(ClickCollect);
            Controller.OnChangeTimeEntity -= ChangeTimeEntityReaction;
        }
        
        
        private void ClickCollect()
        {
            Controller.CollectEntity(_cellId);
        }

        private void ChangeTimeEntityReaction(int obj)
        {
            if (_cellId != obj)
                return;
            
            ChangeProgress(_cellId);
        }

        private void ChangeProgress(int cellId)
        {
            var entity = Controller.GetEntity(cellId);
            if (entity.Time >= entity.Config.TimeToEnd)
            {
                //todo не помню проверяет ли SetActive на то включен обьект или нет, если нет то надо бы переписать
                textProgress.gameObject.SetActive(false);
                imageProgress.gameObject.SetActive(false);
                if (entity.Config.IsCanCollect)
                {
                    collectButton.gameObject.SetActive(true);
                    textButton.text = entity.Config.RewardText;
                    if (_spawnedObject == null)
                    {
                        _spawnedObject = Instantiate(entity.Config.RewardView, spawnContent);
                    }
                    if(_spawnedObject)_spawnedObject.gameObject.SetActive(true);
                }
            }
            else
            {
                if(_spawnedObject)_spawnedObject.gameObject.SetActive(false);
                collectButton.gameObject.SetActive(false);
                textProgress.gameObject.SetActive(true);
                imageProgress.gameObject.SetActive(true);
                imageProgress.fillAmount = entity.Time / entity.Config.TimeToEnd;
                textProgress.text = $"{(int)entity.Time}/{entity.Config.TimeToEnd}";
            }
            
        }
        
        public void Repaint(int id)
        {
            _cellId = id;
            textProgress.gameObject.SetActive(false);
            imageProgress.gameObject.SetActive(false);
            collectButton.gameObject.SetActive(false);
        }
    }
}