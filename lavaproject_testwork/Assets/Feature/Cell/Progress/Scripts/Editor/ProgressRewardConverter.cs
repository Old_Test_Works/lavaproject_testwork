﻿using Editor.Converter;
using UnityEngine;

namespace Feature.Cell.Progress.Editor
{
    [CreateAssetMenu(menuName = "Feature/Cell/Progress/ProgressRewardConverter", fileName = "ProgressRewardConverter", order = 0)]
    public class ProgressRewardConverter : AObjectConverter<ProgressRewardView>
    {

    }
}