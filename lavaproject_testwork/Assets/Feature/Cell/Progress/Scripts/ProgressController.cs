﻿using System;
using System.Collections.Generic;
using Feature.Cell.CellPosition;
using Feature.Cell.Construction.Construction.Scripts;
using Feature.Currency.Scripts;
using Feature.Player.Scripts;
using Managers;
using MCOV;
using UnityEngine;

namespace Feature.Cell.Progress
{
    public class ProgressController : AController
    {
        [SerializeField]
        private ProgressConfigs configs;
        
        private ConstructionController _constructionController;
        private CurrencyChangerController _currencyChangerController;
        private PlayerController _playerController;
        private CellPositionController _cellPositionController;
        public event Action<int> OnChangeTimeEntity;

        private Dictionary<int, ProgressEntity> _loadedEntity = new Dictionary<int, ProgressEntity>();
        public override void Inject(ControllerManager controllerManager)
        {
            _constructionController = controllerManager.GetController<ConstructionController>();
            _currencyChangerController = controllerManager.GetController<CurrencyChangerController>();
            _playerController = controllerManager.GetController<PlayerController>();
            _cellPositionController = controllerManager.GetController<CellPositionController>();
        }
        

        private void Update()
        {
            foreach (var data in _constructionController.GetAllData())
            {
                var entity = GetOrCreateEntity(data);
                if (!entity.IsEnd)
                {
                    entity.Time += Time.deltaTime;
                    OnChangeTimeEntity?.Invoke(entity.CellId);
                    if (entity.Time >= entity.Config.TimeToEnd)
                    {
                        EndEntity(entity);
                    } 
                }
            }
        }
        
        
        public void CollectEntity(int cellId)
        {
            if (!_loadedEntity.ContainsKey(cellId))
            {
                throw new Exception($"Ошибка тут");
            }
            var entity = _loadedEntity[cellId];
            var position = _cellPositionController.GetCellPosition(cellId);
            Action action = delegate { AddReward(entity); };
            _playerController.MoveTo(position.Position, action);
           
            
        }

        private void AddReward(ProgressEntity entity)
        {
            entity.IsEnd = false;
            entity.Time = 0;
            _currencyChangerController.AddCurrency(entity.Config.RewardCollect);
        }

        public bool IsHaveEntity(int cellId)
        {
            return _loadedEntity.ContainsKey(cellId);
        }

        public IProgressConfig GetEntity(int cellId)
        {
            return _loadedEntity[cellId];
        }


        private void EndEntity(ProgressEntity entity)
        {
            
            _currencyChangerController.AddCurrency(entity.Config.RewardEnd);
            entity.IsEnd = true;
        }

        private ProgressEntity GetOrCreateEntity(IConstructionData data)
        {
            if (_loadedEntity.ContainsKey(data.CellId))
                return _loadedEntity[data.CellId];

            var entity = new ProgressEntity();
            entity.CellId = data.CellId;
            entity.Time = 0;
            entity.IsEnd = false;
            entity.Config = configs.Configs[data.ConstructionId];
            _loadedEntity.Add(data.CellId,entity);
            return entity;
        }

    }

    public class ProgressEntity : IProgressConfig
    {
        public int CellId { get; set; }
        public float Time { get; set; }
        public bool IsEnd { get; set; }
        public ProgressConfig Config { get; set; }
    }

    public interface IProgressConfig
    {
        public int CellId { get; }
        public float Time { get; }
        public bool IsEnd { get; }
        public ProgressConfig Config { get; }
    }

    
}