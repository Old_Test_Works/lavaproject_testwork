﻿using System;
using AssetConfigs;
using DefaultObjects;
using UnityEngine;

namespace Feature.Cell.Progress
{

    [CreateAssetMenu(menuName = "Feature/Cell/Progress/ProgressConfigs", fileName = "ProgressConfigs", order = 0)]
    public class ProgressConfigs : AAssetConfigs<ProgressConfig>
    {
    }

    [Serializable]
    public class ProgressConfig : AConfig
    {
        [SerializeField] private int constructionId;

        [SerializeField] private IdIntPara rewardEnd;

        [SerializeField] private IdIntPara rewardCollect;

        [SerializeField] private float timeToEnd;

        [SerializeField] private bool isCanCollect;

        [SerializeField] private string actionName;

        [SerializeField] private ProgressRewardView rewardView;

        [SerializeField] private string rewardText;


        public int ConstructionId => constructionId;
        public IdIntPara RewardEnd => rewardEnd;
        public IdIntPara RewardCollect => rewardCollect;
        public float TimeToEnd => timeToEnd; 
        public bool IsCanCollect => isCanCollect;
        public string ActionName => actionName;
        public ProgressRewardView RewardView => rewardView;
        public string RewardText => rewardText;
    }
}