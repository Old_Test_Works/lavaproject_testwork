﻿using System.Collections.Generic;
using MCOV;
using UnityEngine;

namespace Feature.Cell.Cell.Scripts
{
    public class CellView : AView
    {
        public int Id { get; private set; }

        private List<ICellObject> _cellObjects = new List<ICellObject>();

        protected override void Initialize()
        {
            _cellObjects.Clear();
            //TODO Костыль, возможно нужно перейти к SerializeReference
            GetComponentsInChildren(_cellObjects);
        }

        public void Repaint(int id)
        {
            Id = id;
            foreach (var celObject in _cellObjects)
            {
                celObject.Repaint(id);
            }
        }

    }

    public interface ICellObject
    {
        public void Repaint(int id);
    }
}