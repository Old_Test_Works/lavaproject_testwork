﻿using System;
using System.Collections.Generic;
using MCOV;
using UnityEngine;

namespace Feature.Cell.Cell.Scripts
{
    public class CellController : AController
    {
        //Todo потом добавить конфиг  Уровень игрока == Кол-во ячеек
        [SerializeField]
        private int countCells = 10;
        
        public event Action OnChangeCells;

        private List<int> _loadedCell;

        public IReadOnlyList<int> GetCells()
        {
            if (_loadedCell == null)
            {
                LoadCell();
            }

            return _loadedCell;
        }

        private void LoadCell()
        {
            _loadedCell = new List<int>();

            for (int i = 0; i < countCells; i++)
            {
                _loadedCell.Add(i);
            }
        }
    }


}
