﻿using System.Collections.Generic;
using MCOV;
using UnityEngine;

namespace Feature.Cell.Cell.Scripts
{


    public class CellSpawnerObject : AObject<CellController>
    {
        [SerializeField] private CellView prefabView;

        [SerializeField] private Transform spawnContent;

        private List<CellView> _spawnedCells = new List<CellView>();

        protected override void Show()
        {
            Repaint();
        }

        protected override void Subscribe()
        {
            Controller.OnChangeCells += Repaint;
        }

        protected override void Unsubscribe()
        {
            Controller.OnChangeCells -= Repaint;
        }

        private void Repaint()
        {
            var cells = Controller.GetCells();

            foreach (var cellId in cells)
            {
                if (IsSpawnedViewWithId(cellId))
                    continue;

                SpawnCell(cellId);
            }
        }

        private void SpawnCell(int cellId)
        {
            var view = Instantiate(prefabView, spawnContent);
            view.Repaint(cellId);
            _spawnedCells.Add(view);
        }

        private bool IsSpawnedViewWithId(int cellId)
        {
            foreach (var view in _spawnedCells)
            {
                if (view.Id == cellId)
                    return true;
            }

            return false;
        }
    }
}