﻿using System.Collections.Generic;
using Feature.Position;
using Feature.Position.Scripts;
using Managers;
using MCOV;
using UnityEngine;

namespace Feature.Cell.CellPosition
{
    public class CellPositionController : AController
    {
        private IPositionCalculator _gridPositionCalculator;
        
        //TODO Скорее всего нет смысла кешировать эти данные, тк не так часто спрашивается позиция
        private Dictionary<int, PositionEntity> _loadedEntity = new Dictionary<int, PositionEntity>();

        public override void Inject(ControllerManager controllerManager)
        {
            _gridPositionCalculator = (IPositionCalculator)controllerManager.GetControllerForInterface<IPositionCalculator>();

        }
        
        public IPositionEntity GetCellPosition(int cellId)
        {
            if (_loadedEntity.ContainsKey(cellId))
                return _loadedEntity[cellId];

            var entity = new PositionEntity();
            //Todo по идее нужно делать связть cellId -> id позиции, но пока нет смысла 
            entity.Position = _gridPositionCalculator.CalculatePosition(cellId);
            _loadedEntity.Add(cellId, entity);
            return entity;
        }
    }


    //Вдруг мы захотим добавить логику позиции
    public struct PositionEntity : IPositionEntity
    {
        public Vector3 Position { set; get; }
    }

    public interface IPositionEntity
    {
        public Vector3 Position { get; }
    }
}

