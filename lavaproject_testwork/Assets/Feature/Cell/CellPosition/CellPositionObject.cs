﻿using Feature.Cell.Cell.Scripts;
using MCOV;
using UnityEngine;

namespace Feature.Cell.CellPosition
{
    public class CellPositionObject : AObject<CellPositionController>, ICellObject
    {
        [SerializeField]
        private Transform transform;
        
        public void Repaint(int id)
        {
            var entity = Controller.GetCellPosition(id);
            transform.position = entity.Position;
        }
    }
}