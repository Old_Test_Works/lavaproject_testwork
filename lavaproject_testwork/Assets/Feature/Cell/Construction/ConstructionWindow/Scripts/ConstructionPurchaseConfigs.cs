﻿using System;
using AssetConfigs;
using DefaultObjects;
using Feature.Cell.Construction.Construction.Scripts;
using UnityEngine;

namespace Feature.Cell.Construction.ConstructionWindow.Scripts
{

    [CreateAssetMenu(menuName = "Feature/Cell/Construction/ConstructionPurchaseConfigs",
        fileName = "ConstructionPurchaseConfigs", order = 0)]
    public class ConstructionPurchaseConfigs : AAssetConfigs<ConstructionPurchaseConfig>
    {
    }

    [Serializable]
    public class ConstructionPurchaseConfig : AConfig
    {
        [SerializeField] private int constructionId;

        [SerializeField] private IdIntPara price;

        public int ConstructionId => constructionId;

        public IdIntPara Price => price;

    }
}