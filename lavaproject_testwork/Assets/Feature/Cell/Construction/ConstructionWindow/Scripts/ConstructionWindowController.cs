﻿using System;
using System.Collections.Generic;
using Feature.Cell.CellPosition;
using Feature.Cell.Construction.Construction.Scripts;
using Feature.Currency.Scripts;
using Feature.Player.Scripts;
using Managers;
using MCOV;
using UnityEngine;

namespace Feature.Cell.Construction.ConstructionWindow.Scripts
{
    public class ConstructionWindowController : AController
    {
        [SerializeField]
        private ConstructionPurchaseConfigs configs;
        
        public event Action OnNeedOpenWindow;
        
        public event Action OnNeedCloseWindow;
        
        public event Action<int> OnChangeConstruction;
        
        private ConstructionController _constructionController;

        private CurrencyChangerController _currencyChangerController;
        
        private PlayerController _playerController;

        private CellPositionController _cellPositionController;
        

        private int _currentConstruction;
        public override void Inject(ControllerManager controllerManager)
        {
            _constructionController = controllerManager.GetController<ConstructionController>();
            _currencyChangerController = controllerManager.GetController<CurrencyChangerController>();
            _playerController = controllerManager.GetController<PlayerController>();
            _cellPositionController = controllerManager.GetController<CellPositionController>();
        }

        public bool IsHaveConstruction(int cellId)
        {
            return _constructionController.IsHaveConstructionInCell(cellId);
        }

        public void OpenWindow(int cellId)
        {
            _currentConstruction = cellId;
            OnNeedOpenWindow?.Invoke();
        }

        //todo на случай если захоти привязать аналитику на закрытие окна
        public void CloseWindow()
        {
            OnNeedCloseWindow?.Invoke();
        }

        public IReadOnlyList<ConstructionPurchaseConfig> GetListCurrentPurchase()
        {
            return configs.Configs;
        }

        public void ClickBuyConstruction(ConstructionPurchaseConfig config)
        {
            if (IsCanBuildAndBuy(config))
            {
                CloseWindow();
                Action action =  delegate { BuyConstruction(config); };
                var pos = _cellPositionController.GetCellPosition(_currentConstruction);
                _playerController.MoveTo(pos.Position,action);
            }
        }

        private void BuyConstruction(ConstructionPurchaseConfig config)
        {
            if (IsCanBuildAndBuy(config))
            {
                _currencyChangerController.SubstractCurrency(config.Price);
                _constructionController.SetConstructionInCell(_currentConstruction, config.ConstructionId);
                OnChangeConstruction?.Invoke(_currentConstruction);
            }
        }

        private bool IsCanBuildAndBuy(ConstructionPurchaseConfig config)
        {
            var isCanBuy = _currencyChangerController.IsHaveNecessaryValue(config.Price);
            var isCanBuild = !IsHaveConstruction(_currentConstruction);
            return isCanBuy && isCanBuild;
        }
    }

}