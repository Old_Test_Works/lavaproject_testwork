﻿using Feature.Cell.Cell.Scripts;
using MCOV;
using UnityEngine;
using UnityEngine.UI;

namespace Feature.Cell.Construction.ConstructionWindow.Scripts
{

    public class CellConstructionWindowObject : AObject<ConstructionWindowController>, ICellObject
    {
        [SerializeField]
        private Button openWindowButton;

        private int _cellId;

        protected override void Subscribe()
        {
            openWindowButton.onClick.AddListener(ClickOpenButton);
            Controller.OnChangeConstruction += ChangeConstructionReaction;
        }



        protected override void Unsubscribe()
        {
            openWindowButton.onClick.RemoveListener(ClickOpenButton);
            Controller.OnChangeConstruction -= ChangeConstructionReaction;
        }

        public void Repaint(int id)
        {
            _cellId = id;
            openWindowButton.gameObject.SetActive(!Controller.IsHaveConstruction(_cellId));
        }
        
        private void ChangeConstructionReaction(int cellId)
        {
            if (_cellId == cellId)
                Repaint(_cellId);
        }
        
        private void ClickOpenButton()
        {
            Controller.OpenWindow(_cellId);
        }
    }
}