﻿using Feature.Cell.Construction.Construction.Scripts;
using Feature.Currency.Scripts;
using MCOV;
using UnityEngine;
using UnityEngine.UI;

namespace Feature.Cell.Construction.ConstructionWindow.Scripts
{


    public class ConstructionPurchaseObject : AObject<ConstructionWindowController>
    {
        [SerializeField] private CurrencyPriceCurrentObject priceObject;

        [SerializeField] private ConstructionObject constructionObject;

        [SerializeField] private Button buyButton;

        private ConstructionPurchaseConfig _config;

        protected override void Subscribe()
        {
            buyButton.onClick.AddListener(ClickBuy);
        }


        protected override void Unsubscribe()
        {
            buyButton.onClick.RemoveListener(ClickBuy);
        }

        public void Repaint(ConstructionPurchaseConfig config)
        {
            _config = config;
            constructionObject.Repaint(_config.ConstructionId);
            priceObject.SetPrice(config.Price);
        }

        private void ClickBuy()
        {
            Controller.ClickBuyConstruction(_config);
        }

    }
}