﻿using System.Collections.Generic;
using MCOV;
using UnityEngine;
using UnityEngine.UI;

namespace Feature.Cell.Construction.ConstructionWindow.Scripts
{
    public class ConstructionWindowObject : AObject<ConstructionWindowController>
    {
        [SerializeField]
        private GameObject panel;
        
        [SerializeField]
        private Transform content;
        
        [SerializeField]
        private Button closeButton;

        [SerializeField]
        private ConstructionPurchaseObject prefabItem;

        private List<ConstructionPurchaseObject> _spawnedItems = new List<ConstructionPurchaseObject>();

        protected override void Subscribe()
        {
            Controller.OnNeedOpenWindow += ShowWindow;
            Controller.OnNeedCloseWindow += CloseWindow;
            closeButton.onClick.AddListener(ClickClose);
        }



        protected override void Unsubscribe()
        {
            Controller.OnNeedOpenWindow -= ShowWindow;
            Controller.OnNeedCloseWindow -= CloseWindow;
            closeButton.onClick.RemoveListener(ClickClose);
        }

        private void CloseWindow()
        {
            panel.gameObject.SetActive(false);
        }
        
        private void ShowWindow()
        {
            panel.gameObject.SetActive(true);
            var configs = Controller.GetListCurrentPurchase();
            var delta = configs.Count - _spawnedItems.Count;

            for (int i = 0; i < delta; i++)
            {
                var view = Instantiate(prefabItem, content);
                _spawnedItems.Add(view);
            }

            for (int i = 0; i < _spawnedItems.Count; i++)
            {
                if (i >= configs.Count)
                {
                    _spawnedItems[i].gameObject.SetActive(false);
                    continue;
                }
                _spawnedItems[i].gameObject.SetActive(true);
                _spawnedItems[i].Repaint(configs[i]);
            }

        }
        
        private void ClickClose()
        {
            Controller.CloseWindow();
        }
    }
}