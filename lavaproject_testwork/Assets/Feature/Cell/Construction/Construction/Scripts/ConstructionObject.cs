﻿using MCOV;
using UnityEngine;
using UnityEngine.UI;

namespace Feature.Cell.Construction.Construction.Scripts
{
    public class ConstructionObject : AObject<ConstructionController>
    {
        [SerializeField]
        private Text nameText;

        public void Repaint(int constructionId)
        {
            var config = Controller.GetConfig(constructionId);
            nameText.text = config.ConstructionName;
        }
    }
}