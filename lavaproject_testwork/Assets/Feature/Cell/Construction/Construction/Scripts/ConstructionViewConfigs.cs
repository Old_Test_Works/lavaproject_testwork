﻿using System;
using AssetConfigs;
using UnityEngine;
using UnityEngine.Serialization;

namespace Feature.Cell.Construction.Construction.Scripts
{
    [CreateAssetMenu(menuName = "Feature/Cell/Construction/ConstructionViewAsset", fileName = "ConstructionViewAsset", order = 0)]
    public class ConstructionViewConfigs : AAssetConfigs<ConstructionViewConfig>
    {
    }
    
    [Serializable]
    public class ConstructionViewConfig : AConfig
    {
        [SerializeField]
        private int constructionId;
        
        [SerializeField]
        private string constructionName;

        [SerializeField]
        private CellConstructionView constructionView;
        
        public int ConstructionId => constructionId;
        
        public string ConstructionName => constructionName;
        
        public CellConstructionView ConstructionView => constructionView;
        
    }
}