﻿using Editor.Converter;
using UnityEngine;

namespace Feature.Cell.Construction.Construction.Scripts.Editor
{
    [CreateAssetMenu(menuName = "Feature/Cell/ConstructionViewProvider", fileName = "ConstructionViewProvider", order = 0)]
    public class ConstructionViewProvider : AObjectConverter<CellConstructionView>
    {

    }
}