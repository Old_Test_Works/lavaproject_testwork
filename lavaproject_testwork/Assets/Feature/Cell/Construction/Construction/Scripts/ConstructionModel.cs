﻿using System;
using System.Collections.Generic;
using MCOV;

namespace Feature.Cell.Construction.Construction.Scripts
{
    public class ConstructionModel : AModel
    {
        private ConstructionDates _data;

        public ConstructionDates Data
        {
            get
            {
                if (_data != null)
                    return _data;
                _data = LoadData<ConstructionDates>();
                return _data;
            }
        }

    }

    [Serializable]
    public class ConstructionDates : AData
    {
        public List<ConstructionData> ListBuild = new List<ConstructionData>();
    }

    [Serializable]
    public class ConstructionData : IConstructionData
    {
        public int cellId;
        public int constructionId;
        public int CellId => cellId;
        public int ConstructionId => constructionId;
    }

    public interface IConstructionData
    {
        public int CellId { get;  }
        public int ConstructionId { get;  } 
    }
}