﻿using System;
using System.Collections.Generic;
using MCOV;
using UnityEngine;

namespace Feature.Cell.Construction.Construction.Scripts
{
    public class ConstructionController : AController
    {
        [SerializeField]
        private ConstructionModel model;

        [SerializeField]
        private ConstructionViewConfigs configs;

        public event Action<int> OnChangeConstruction;
        
        public IReadOnlyList<IConstructionData> GetAllData()
        {
           return model.Data.ListBuild;
        }
        
        public bool IsHaveConstructionInCell(int cellId)
        {
            return GetData(cellId) != null;
        }

        public void SetConstructionInCell(int cellId, int constructionId)
        {
            var data = GetData(cellId);
            if (data != null)
            {
                data.constructionId = constructionId;
                OnChangeConstruction?.Invoke(cellId);
                return; 
            }

            var newData = new ConstructionData
            {
                cellId = cellId,
                constructionId = constructionId
            };
            model.Data.ListBuild.Add(newData);
            OnChangeConstruction?.Invoke(cellId);
        }

        public ConstructionViewConfig GetConfig(int constructionId)
        {
            return configs.Configs[constructionId];
        }

        public IConstructionEntity GetEntityInConstruction(int cellId)
        {
            var data = GetData(cellId);
            if (data == null)
                throw new Exception($"Попытка получить дату который нет {cellId}");

            var entity = new ConstructionEntity();

            entity.Config = configs.Configs[data.ConstructionId];
            return entity;
        }

        private ConstructionData GetData(int cellId)
        {
            foreach (var data in model.Data.ListBuild)
            {
                if (data.CellId == cellId)
                    return data;
            }

            return null;
        }


    }

    public struct ConstructionEntity : IConstructionEntity
    {
        public ConstructionViewConfig Config { set; get; }
    }
    
    public interface IConstructionEntity
    {
        public ConstructionViewConfig Config { get; }
    }


}