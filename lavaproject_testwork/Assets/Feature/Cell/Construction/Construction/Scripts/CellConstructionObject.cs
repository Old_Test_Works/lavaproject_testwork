﻿using Feature.Cell.Cell.Scripts;
using MCOV;
using UnityEngine;
using UnityEngine.UI;

namespace Feature.Cell.Construction.Construction.Scripts
{
    public class CellConstructionObject : AObject<ConstructionController>, ICellObject
    {
        [SerializeField]
        private Transform contentForSpawn;

        [SerializeField]
        private Text textName;
        
        
        private CellConstructionView _view;
        
        private int _cellId = -1;

        protected override void Subscribe()
        {
            Controller.OnChangeConstruction += ChangeConstructionReaction;
        }

        protected override void Unsubscribe()
        {
            Controller.OnChangeConstruction -= ChangeConstructionReaction;
        }

        public void Repaint(int cellId)
        {
            _cellId = cellId;
            if (Controller.IsHaveConstructionInCell(_cellId))
            {
                var entity = Controller.GetEntityInConstruction(_cellId);
                _view = Instantiate(entity.Config.ConstructionView, contentForSpawn);
                _view.Repaint(_cellId);
                textName.text = entity.Config.ConstructionName;

            }
            else
            {
                textName.text = "";
                if(_view)
                    Destroy(_view.gameObject);
            }
        }

        private void ChangeConstructionReaction(int cellId)
        {
            if (cellId == _cellId)
                Repaint(cellId);
        }
    }
}