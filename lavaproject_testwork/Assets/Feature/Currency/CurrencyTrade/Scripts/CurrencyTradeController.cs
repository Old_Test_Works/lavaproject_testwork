﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using AssetConfigs;
using DefaultObjects;
using Feature.Currency.Scripts;
using Managers;
using MCOV;
using UnityEngine;

namespace Feature.Currency.CurrencyTrade.Scripts
{


    public class CurrencyTradeController : AController
    {
        [SerializeField] private CurrencyTradeConfigs asset;

        private CurrencyController _currencyController;

        private CurrencyChangerController _currencyChangerController;

        public Action OnNeedRepaintPanel { get; set; }

        public override void Inject(ControllerManager controllerManager)
        {
            _currencyController = controllerManager.GetController<CurrencyController>();
            _currencyChangerController = controllerManager.GetController<CurrencyChangerController>();
        }

        public override void Subscribe()
        {
            _currencyController.OnChangeCurrency += ChangeCurrencyReaction;
        }

        public override void Unsubscribe()
        {
            _currencyController.OnChangeCurrency -= ChangeCurrencyReaction;
        }

        private void ChangeCurrencyReaction(int obj)
        {
            OnNeedRepaintPanel?.Invoke();
        }

        public IReadOnlyList<int> CurrencyForTrade()
        {
            return asset.Configs
                .Where(x => x.IsCanTrade)
                .Select(x => x.CurrencyId)
                .ToList();
        }

        public bool IsNeedShowCurrency(int idCurrency)
        {
            return _currencyController.GetCurrencyEntity(idCurrency).CurrentValue > 0;
        }

        public void TradeCurrency(int currencyId)
        {
            var entity = _currencyController.GetCurrencyEntity(currencyId);
            var config = asset.Configs[currencyId];
            _currencyChangerController.AddCurrency(config.Price.ID,config.Price.Value * entity.CurrentValue);
            _currencyChangerController.SubstractCurrency(currencyId, entity.CurrentValue);
            
        }
    }
}
