﻿using System;
using AssetConfigs;
using DefaultObjects;
using UnityEngine;

namespace Feature.Currency.CurrencyTrade.Scripts
{
    [CreateAssetMenu(menuName = "Feature/Currency/CurrencyTradeConfigs", fileName = "CurrencyTradeConfigs", order = 0)]
    public class CurrencyTradeConfigs : AAssetConfigs<CurrencyTradeConfig>
    {
    }
    
    [Serializable]
    public class CurrencyTradeConfig : AConfig
    {
        [SerializeField]
        private int currencyId;
        
        [SerializeField]
        private IdIntPara price;

        [SerializeField]
        private bool isCanTrade;

        public int CurrencyId => currencyId;
        
        public IdIntPara Price => price;
        
        public bool IsCanTrade => isCanTrade;
    }
}