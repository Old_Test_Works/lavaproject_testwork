﻿using System.Collections.Generic;
using MCOV;
using UnityEngine;

namespace Feature.Currency.CurrencyTrade.Scripts
{
    public class CurrencyTradePanelObject : AObject<CurrencyTradeController>
    {
        [SerializeField] private CurrencyTradeObject prefabObject;

        [SerializeField] private Transform spawnContent;

        private List<CurrencyTradeObject> _spawnedCells = new List<CurrencyTradeObject>();

        protected override void Show()
        {
            SpawnAllViews();
        }


        protected override void Subscribe()
        {
            Controller.OnNeedRepaintPanel += Repaint;
        }

        protected override void Unsubscribe()
        {
            Controller.OnNeedRepaintPanel -= Repaint;
        }

        //todo можно намного лучше сделать если останется время надо пределать 
        private void SpawnAllViews()
        {
            var currencyIds = Controller.CurrencyForTrade();

            foreach (var id in currencyIds)
            {
                var view = Instantiate(prefabObject, spawnContent);
                view.Repaint(id);
                RepaintView(view);
                _spawnedCells.Add(view);

            }
        }


        private void Repaint()
        {
            foreach (var view in _spawnedCells)
            {
                RepaintView(view);
            }
        }

        private void RepaintView(CurrencyTradeObject view)
        {
            var isNeedShow = Controller.IsNeedShowCurrency(view.CurrencyId);
            if (isNeedShow)
            {
                if (!view.gameObject.activeSelf)
                    view.gameObject.SetActive(true);

                view.Repaint(view.CurrencyId);
            }
            else
            {
                if (view.gameObject.activeSelf)
                    view.gameObject.SetActive(false);
            }
        }



    }
}