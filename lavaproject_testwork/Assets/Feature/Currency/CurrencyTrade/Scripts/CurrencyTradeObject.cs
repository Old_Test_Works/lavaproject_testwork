﻿using Feature.Currency.Scripts;
using MCOV;
using UnityEngine;
using UnityEngine.UI;

namespace Feature.Currency.CurrencyTrade.Scripts
{
    public class CurrencyTradeObject : AObject<CurrencyTradeController>
    {

        [SerializeField] private Button sellButton;
        
        [SerializeField] private CurrencyObject currencyObject;

        public int CurrencyId { get; private set; }

        protected override void Subscribe()
        {
            sellButton.onClick.AddListener(ClickSellButton);
        }

        protected override void Unsubscribe()
        {
            sellButton.onClick.AddListener(ClickSellButton);
        }


        public void Repaint(int currencyId)
        {
            CurrencyId = currencyId;
            currencyObject.Repaint(currencyId);
        }
        
        private void ClickSellButton()
        {
            Controller.TradeCurrency(CurrencyId);
        }
    }
}