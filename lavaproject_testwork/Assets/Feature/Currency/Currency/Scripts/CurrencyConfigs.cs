﻿using System;
using System.Collections.Generic;
using AssetConfigs;
using UnityEngine;

namespace Feature.Currency.Scripts
{
    [CreateAssetMenu(menuName = "Feature/Currency/CurrencyConfigs", fileName = "CurrencyConfigs", order = 0)]
    public class CurrencyConfigs: AAssetConfigs<CurrencyConfig>
    {
    }

    [Serializable]
    public class CurrencyConfig  : AConfig
    {
        
        [SerializeField]
        private int currencyId;

        [SerializeField]
        private Sprite currencyIcon;

        [SerializeField]
        private int defaultValue;

        public int CurrencyId => currencyId;

        public Sprite CurrencyIcon => currencyIcon;

        public int DefaultValue => defaultValue;
        

    }
}