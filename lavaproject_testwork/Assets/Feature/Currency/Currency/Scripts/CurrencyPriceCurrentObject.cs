﻿using DefaultObjects;
using Feature.Currency.Currency.Scripts;
using MCOV;
using UnityEngine;
using UnityEngine.UI;

namespace Feature.Currency.Scripts
{
    public class CurrencyPriceCurrentObject : AObject<CurrencyController>
    {
        [SerializeField]
        private Text textPrice;
        [SerializeField]
        private Color goodColor;
        
        [SerializeField]
        private Color badColor;
        
        [SerializeField]
        private Image iconCurrency;

        private IdIntPara _idIntPara;
        
        protected override void Subscribe()
        {
            Controller.OnChangeCurrency += ChangeValue;
        }

        protected override void Unsubscribe()
        {
            Controller.OnChangeCurrency -= ChangeValue;
        }

        public void SetPrice(IdIntPara intPara)
        {
            _idIntPara = intPara;
            Repaint();
        }

        private void Repaint()
        {
            var entity = Controller.GetCurrencyEntity(_idIntPara.ID);
            
            iconCurrency.sprite = entity.Config.CurrencyIcon;
            
            textPrice.text = _idIntPara.Value.ToString();
            if (entity.CurrentValue >= _idIntPara.Value)
            {
                textPrice.color = goodColor;
            }
            else
            {
                textPrice.color = badColor;
            }
        }
        
        private void ChangeValue(int idCurrency)
        {
            if(_idIntPara == null)
                return;
            
            if (idCurrency != _idIntPara.ID)
                return;
            
            Repaint();
        }
    }
}