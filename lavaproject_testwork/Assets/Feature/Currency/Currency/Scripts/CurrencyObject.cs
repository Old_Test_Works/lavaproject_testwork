﻿using MCOV;
using UnityEngine;
using UnityEngine.UI;

namespace Feature.Currency.Scripts
{
    public class CurrencyObject : AObject<CurrencyController>
    {
        
        [SerializeField]
        private int idCurrency;
        
        [SerializeField]
        private Image iconCurrency;
        
        [SerializeField]
        private Text textCurrency;
        

        private ICurrencyEntity _currentEntity;

        protected override void Show()
        {
            _currentEntity = Controller.GetCurrencyEntity(idCurrency);
            
            iconCurrency.sprite = _currentEntity.Config.CurrencyIcon;
            Repaint();
        }

        protected override void Subscribe()
        {
            Controller.OnChangeCurrency += ChangeCurrency;
        }

        protected override void Unsubscribe()
        {
            Controller.OnChangeCurrency -= ChangeCurrency;
        }

        public void Repaint(int id)
        {
            idCurrency = id;
            _currentEntity = Controller.GetCurrencyEntity(idCurrency);
            
            iconCurrency.sprite = _currentEntity.Config.CurrencyIcon;
            Repaint();
        }
        
        private void ChangeCurrency(int id)
        {
            if (idCurrency == id)
            {
                Repaint();
            }
        }



        private void Repaint()
        {
            textCurrency.text = _currentEntity.CurrentValue.ToString();
        }
    }
    
    
}