﻿using System;
using System.Collections.Generic;
using MCOV;

namespace Feature.Currency.Currency.Scripts
{
    public class CurrencyModel : AModel
    {
        private CurrencyListData _data;
        
        public CurrencyListData CurrencyListData
        {
            get
            {
                if (_data == null)
                {
                    _data = LoadData<CurrencyListData>();
                }

                return _data;
            }
        }
        
    }
    
    
    [Serializable]
    public class CurrencyListData : AData
    {
        public List<CurrencyData> Data;
    }
    
    [Serializable]
    public class CurrencyData
    {
        public int CurrencyId;
        public int CurrencyValue;
    }
}