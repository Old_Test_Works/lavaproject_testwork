﻿using DefaultObjects;
using MCOV;
using UnityEngine;

namespace Feature.Currency.Scripts
{
    public class CurrencyChangerController : AController
    {
        [SerializeField]
        private CurrencyController currencyController;

        
        public bool IsHaveNecessaryValue(IdIntPara pricePara)
        {
            return IsHaveNecessaryValue(pricePara.ID, pricePara.Value);
        }

        public bool IsHaveNecessaryValue(int idCurrency, int value)
        {
            var entity = currencyController.GetCurrencyEntity(idCurrency);
            return entity.CurrentValue >= value;
        }


        public void AddCurrency(IdIntPara pricePara)
        {
            AddCurrency(pricePara.ID, pricePara.Value);
        }

        public void AddCurrency(int idCurrency, int addValue)
        {
            var entity = currencyController.GetCurrencyEntity(idCurrency);
            var resultValue = entity.CurrentValue + addValue;
            currencyController.SetCurrencyValue(idCurrency, resultValue);
        }
        
        public void SubstractCurrency(IdIntPara pricePara)
        {
            SubstractCurrency(pricePara.ID, pricePara.Value);
        }
        
        public void SubstractCurrency(int idCurrency, int addValue)
        {
            var entity = currencyController.GetCurrencyEntity(idCurrency);
            var resultValue = entity.CurrentValue - addValue;
            currencyController.SetCurrencyValue(idCurrency, resultValue);
        }
    }
}