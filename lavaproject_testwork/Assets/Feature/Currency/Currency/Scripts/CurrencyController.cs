﻿using System;
using System.Collections.Generic;
using Feature.Currency.Currency.Scripts;
using MCOV;
using UnityEngine;

namespace Feature.Currency.Scripts
{
    public class CurrencyController : AController
    {
        [SerializeField]
        private CurrencyModel model;


        [SerializeField]
        private CurrencyConfigs currencyConfigs;
        
        public event Action<int> OnChangeCurrency;

        private Dictionary<int, CurrencyEntity> _loadedCurrency = new Dictionary<int, CurrencyEntity>();
        public ICurrencyEntity GetCurrencyEntity(int idCurrency)
        {
            return GetEntity(idCurrency);
        }

        public void SetCurrencyValue(int idCurrency, int value)
        {
            var entity = GetEntity(idCurrency);
            entity.Data.CurrencyValue = value;
            OnChangeCurrency?.Invoke(idCurrency);
        }

        private CurrencyEntity GetEntity(int idCurrency)
        {
            if (_loadedCurrency.ContainsKey(idCurrency))
            {
                return _loadedCurrency[idCurrency];
            }

            CurrencyData currencyData = null;
            var config = currencyConfigs.Configs[idCurrency];

            if (model.CurrencyListData.Data == null)
            {
                model.CurrencyListData.Data = new List<CurrencyData>();
            }

            foreach (var data in model.CurrencyListData.Data)
            {
                if (data.CurrencyId == idCurrency)
                {
                    currencyData = data;
                    break;
                }
            }

            if (currencyData == null)
            {
                var newData = new CurrencyData();
                newData.CurrencyId = idCurrency;
                newData.CurrencyValue = config.DefaultValue;
                model.CurrencyListData.Data.Add(newData);
                currencyData = newData;
            }

            var entity = new CurrencyEntity();
            entity.Config = config;
            entity.Data = currencyData;
            
            _loadedCurrency.Add(idCurrency, entity);

            return entity;

        }
    }

    public class CurrencyEntity : ICurrencyEntity
    {
        public int CurrentValue => Data.CurrencyValue;
        public CurrencyConfig Config { get; set; }

        public CurrencyData Data;
    }

    public interface ICurrencyEntity
    {
        public int CurrentValue { get; }
        public CurrencyConfig Config { get; }
    }
}